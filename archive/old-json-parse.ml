
  let get_json_value key1 key2 json =
    let open Yojson.Basic.Util in
    json
    |> member "query"
    |> member "pages"
    |> values
    |> filter_member key1
    |> flatten
    |> filter_member key2
    |> filter_string
    |> List.hd_exn  

  let potd_json_from_date date =
    Web.get_json (Uri.to_string (wm_images_query_url date)) 
  
  let potd_json_from_name name = 
    Web.get_json (Uri.to_string (wm_imageinfo_query_url name)) 
  
  let potd_image_name date =
    potd_json_from_date date
    |> (get_json_value "images" "title")

  let potd_image_url date =
    potd_image_name date 
    |> potd_json_from_name
    |> (get_json_value "imageinfo" "url")

  let potd_image_descurl date =
    potd_image_name date 
    |> potd_json_from_name
    |> (get_json_value "imageinfo" "descurl")
