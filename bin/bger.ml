
open Backgrounder
(* open Base *)
open Lwt
open Cmdliner

let config () =
  let open Bg in
  get_config ()

let wm_store_count = 10

let sources () =
  let conf = config () in
  let open Bg in
  Base.List.filter conf.background_sources ~f:source_not_empty_p

let delay () =
  let conf = config () in
  conf.sleep_duration

let rec display_loop () =
  let open Choose.Chooser in
  let d = 60.0 *. delay () in
  let%lwt image = sources () |> choose_image in
  Choose.History.append_history image
  >>= Choose.History.history_log 
  >>= Image.display_image d
  >>= display_loop 

let rec reload_loop () =
  let d = 60.0 *. delay () *. Float.of_int wm_store_count /. 2. in
  let%lwt () = Choose.Potd.refill wm_store_count () in
  Lwt_unix.sleep d >>= reload_loop
  
let start () =
  Bg.Config.local_init ();
  Lwt.pick [ display_loop ();
             reload_loop ()]
  |> Lwt_main.run 


let start_term = Term.(const start $ const ())

let cmd =
  let doc = "start" in
  let man = [
      `S Manpage.s_bugs;
      `P "Report bugs to <george>"]
  in
  let info = Cmd.info "bger" ~version:"%%VERSION%%" ~doc ~man in
  Cmd.v info start_term
  

let main () = exit (Cmd.eval cmd)

let () = CCRandom.self_init ();
         main ()

