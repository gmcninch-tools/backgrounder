open Base
include Core_unix

(* open Gm_utils *)

module Users = struct  
  let current_user () =
    Unix.(getuid () |> getpwuid)

  let user_name () =
    Unix.((current_user ()).pw_name)

  let home_directory username =
    let open Unix in
    try
      Some (getpwnam username).pw_dir
    with
      _ -> None

  let home_directory_current () =
    user_name ()
    |> home_directory
    |> Option.value ~default:"/home/george/"
end

module Config = struct
  
  let backgrounder_dir =
    Core.Filename.concat
      (Users.home_directory_current ())
      "assets/config/backgrounder/"

  let config_file = Core.Filename.concat backgrounder_dir "backgrounds.json"

  let local_dir () =
    let default = Core.Filename.concat
                    (Users.home_directory_current ())
                    ".local/share"
    in
    let home = Option.(value ~default @@ Sys.getenv "XDG_DATA_HOME")
    in
    Core.Filename.concat home "gm-wallpapers"

  let history_file () = Core.Filename.concat (local_dir ()) "history.json"
  
  let local_dir_wmc () =
    Core.Filename.concat (local_dir ()) "wmc"

  let local_dir_staging () =
    Core.Filename.concat (local_dir ()) "staging"

  let create_dir_if_needed dirname =
    match Sys_unix.file_exists dirname with
    | `Yes -> ()
    | `No | `Unknown -> Core_unix.mkdir dirname

  let local_init () =
    List.iter ~f:create_dir_if_needed [local_dir ();
                                       local_dir_wmc ();
                                       local_dir_staging ();] ;
    match Sys_unix.file_exists (history_file ()) with
    | `Yes -> ()
    | `No | `Unknown -> Stdio.Out_channel.write_all (history_file ()) ~data:"\n"

end


type source = { use_frequency: float;
                origin:  origin_type;
              }
and desktop_environment = Cinnamon
                        | Gnome
                        (* | XFCE *)
                        | Mate
                        | Generic
and origin_type = Dir_origin of dir_origin_t
                | Wmc_origin of string
and dir_origin_t = { name: string;
                     locations: string list }
and conf  = { sleep_duration: float;
              desktop_environment: desktop_environment;
              background_sources: source list; }

let conf_decoder =
  let open Decoders_ezjsonm.Decode in
  let dir_origin_decoder =
    field "use-frequency" float >>= fun use_frequency ->
    field "name" string >>= fun name ->
    field "base-dir" string >>= fun base_dir ->
    field "locations" (list string) >>= fun location_names ->
    let locations = Base.List.map location_names
                      ~f:(fun n -> Core.Filename.concat base_dir n)
    in
    let origin = Dir_origin {name; locations}
    in
    succeed {use_frequency; origin}
  in
  let wmc_origin_decoder =
    field "use-frequency" float >>= fun use_frequency ->
    field "location" string >>= fun location ->
    let origin = Wmc_origin location in
    succeed {use_frequency; origin}
  in
  let origin_decoder =
    one_of [("local",dir_origin_decoder);
            ("wmc",wmc_origin_decoder);]
  in  
  field "sleep-duration" float >>= fun sleep_duration ->
  field "desktop-environment" string >>= fun sdesktop_environment ->  
  field "background-sources" (list origin_decoder) >>= fun background_sources ->
  let desktop_environment = match sdesktop_environment with
      | "cinnamon" -> Cinnamon
      (* | "xfce" -> XFCE *)
      | "gnome" -> Gnome
      | "mate" -> Mate
      | "generic" -> Generic
      | _ -> Generic
  in  
  succeed { sleep_duration; desktop_environment; background_sources }


let get_config ?(filename=Config.config_file) () =
  let open Decoders_ezjsonm.Decode in
  let open Base.List.Assoc in
  let host = Core_unix.gethostname () in
  let confs = decode_value (key_value_pairs conf_decoder)
              @@ Ezjsonm.from_channel @@ Stdio.In_channel.create filename
  in
  match confs with
  | Ok r -> find_exn r ~equal:String.equal host
  | Error e -> pp_error Stdlib.Format.err_formatter e;
               failwith "failed"
              

let dir_not_empty_p dir =
  match Sys_unix.ls_dir dir with
  | [] -> false
  | _ -> true



let source_not_empty_p source =
  match source.origin with
  | Dir_origin orig ->
     List.fold orig.locations
       ~init:false
       ~f:(fun b x -> b || (dir_not_empty_p x))
  | Wmc_origin location -> dir_not_empty_p location


