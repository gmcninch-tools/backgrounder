
module Config:sig

  val backgrounder_dir: string
  val config_file: string
  val history_file: unit -> string
  val local_dir: unit -> string
  val local_dir_wmc: unit -> string
  val local_dir_staging: unit -> string
  val create_dir_if_needed: string -> unit
  val local_init: unit -> unit
end


type source = { use_frequency: float;
                origin:  origin_type;
              }
and desktop_environment = Cinnamon
                        | Gnome
                        (* | XFCE *)
                        | Mate
                        | Generic
and origin_type = Dir_origin of dir_origin_t
                | Wmc_origin of string
and dir_origin_t = { name: string;
                     locations: string list }
and conf  = { sleep_duration: float;
              desktop_environment: desktop_environment;
              background_sources: source list; }


val get_config: ?filename:string -> unit -> conf

val dir_not_empty_p: string -> bool
val source_not_empty_p: source -> bool
