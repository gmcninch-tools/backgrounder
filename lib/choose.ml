open Base
open Lwt

(* open Gm_utils *)


module History = struct
  
  type t = {image: Image.t;
            display_time: Core.Time_float.t}

  let history_encoder =
    let open Decoders_ezjsonm.Encode in
    fun history ->
    obj [ ("image", Image.image_encoder history.image);
          ("display_time", string @@ Time_float_unix.to_string history.display_time)
      ]

  let history_decoder =
    let open Decoders_ezjsonm.Decode in
    field "image" Image.image_decoder >>= fun image ->
    field "display_time" string >>= fun dts ->
    let display_time = Core.Time_float.of_string_with_utc_offset dts in
    succeed {image; display_time }
  
  let history_of_image   ?(display_time=Core.Time_float.now ())  image =
    {image; display_time}
  
  let newer_than_p ~hours history =
    let open Core.Time_float in
    let s = Span.of_hr hours in
    let d = sub (now ()) s in
    is_later history.display_time ~than:d
    
  let history () =
    let open Decoders_ezjsonm.Decode in
    let rhist = decode_value (list history_decoder)
                @@ Ezjsonm.from_channel
                @@ Stdio.In_channel.create
                @@ Bg.Config.history_file ()
    in
    match rhist with
    | Error _ ->  failwith "failed to parse history json"
    | Ok hist -> return @@ List.filter ~f:(newer_than_p ~hours:24.) hist
  
  
  let history_log image =
    let open Image in
    let src = match image.image_source with
      | Local s -> "Local image; category " ^ s 
      | Wmc_potd potd -> "POTD " ^ (potd.date |> Core.Date.to_string)
    in
    [ src; image.image_name]
    |> String.concat ~sep:" - "
    |> Stdio.print_endline;
    Lwt.return image
  
  let write_history hist =
    let open Ezjsonm in
    let open Decoders_ezjsonm.Encode in
    let data = value_to_string ~minify:false @@ encode_value (list history_encoder) hist in  
    Stdio.Out_channel.write_all (Bg.Config.history_file ())
      ~data;
    Lwt.return ()
  
  let append_history image =
    let%lwt hist = history () in
    (history_of_image image)::hist |> write_history >>= fun () ->
    Lwt.return image

end

module Potd = struct
  module Dj = Decoders_ezjsonm.Decode
  include Wikimedia_commons
  include Util
  
  let all_potd_dates = Date_range.since (Core.Date.of_string "2007-01-01")

  let potd_query_d =
    let open Dj in
    let imaged =
      field "title" string >>= fun title ->
      succeed title
    in
    let page_item =
      field "images" (list imaged) >>= fun images ->
      succeed images
    in
    at ["query"; "pages"] (key_value_pairs_seq (fun _ -> page_item)) >>= fun page_list ->
    succeed (List.concat page_list)


  let potd_image_d name date =
    let open Dj in
    let cleanup_name s =
      let open Base.String.Search_pattern in
      replace_all (create ":") ~in_:s ~with_:"--"
    in
    let image_d =
      field "url" string >>= fun url ->
      field "descriptionurl" string >>= fun desc_url ->
      let image_name = cleanup_name name in
      succeed Image.({image_source = Wmc_potd {date; url; desc_url};
                      image_name;
                      image_dirname = Bg.Config.local_dir_wmc ();
                     })
    in
    let page_item =
      field "imageinfo" (list image_d) >>= fun image_info ->
      succeed image_info
    in
    at ["query"; "pages"] (key_value_pairs_seq  (fun _ -> page_item)) >>= fun page_list ->
    succeed (List.concat page_list)

  let potd_image_list date =
    (* First get the names via a "query" *)
    let potd_url_list_from_name name =
      let%lwt y = Wm_urls.wm_imageinfo_query_url name
                  |> Web.get_json in
      let v = Dj.decode_value (potd_image_d name date) y
              |> Stdlib.Result.get_ok
      in
      Lwt.return v
    in
    let names =
      let%lwt y = Wm_urls.wm_images_query_url date
                  |> Web.get_json
      in
      let v = Dj.decode_value potd_query_d y
              |> Stdlib.Result.get_ok
      in
      Lwt.return v
    in
    let%lwt names = names in
    let%lwt l = Lwt_list.map_p potd_url_list_from_name names in
    Lwt.return (List.concat l)

  exception Potd_no_image of Core.Date.t

  let potd_image date =
    let%lwt dl = potd_image_list date in
    match dl with
    | [] -> Lwt.fail (Potd_no_image date)
    | hd::_ -> Lwt.return hd

  let staging_filename image =
    Image.(image.image_name)
    |> Core.Filename.concat (Bg.Config.local_dir_staging ())

  let count_available () =
    Sys_unix.ls_dir (Bg.Config.local_dir_wmc ())
    |> List.filter ~f:image_file_p
    |> List.length 
  
  let rec refill max () =
    if count_available () < max then
      let ii = List.random_element_exn all_potd_dates |> potd_image
      in
      let dl () =
        let%lwt ii = ii in
        match ii.image_source with
        | Local _ -> Lwt.return @@ Result.Error "local!"
        | Wmc_potd potd -> Web.download_url
                             ~url:(potd.url |> Uri.of_string)
                             ~fn:(staging_filename ii) in
      let mv () =
        let%lwt ii = ii in
        Core_unix.rename
          ~src:(staging_filename ii)
          ~dst:(Image.local_filename ii);
        Image.write_image_json ii
      in
      let%lwt result = dl () in
      (match result with
       | Result.Ok _  -> mv ()
       | Result.Error _ -> Lwt.return ())
      >>= refill max 
    else Lwt.return ()
  
end

module Chooser = struct
  include Bg
  include Image
  include Util

  let random_image_file_from_dirs dir_list =
    let open CCRandom in
    let gen = pick_list (CCList.concat_map images_in_dir dir_list) in
    run gen
  

  let random_json_file_from_dir dir =
    json_in_dir dir
    |> List.random_element_exn
 
  let choose_image_from_source source =
    match source.origin with
    | Dir_origin orig ->     let fn = random_image_file_from_dirs orig.locations in
                             let im = { image_source  = Local orig.name;
                                        image_dirname = Core.Filename.dirname fn;
                                        image_name    = Core.Filename.basename fn}
                             in
                             Lwt.return im
    | Wmc_origin loc -> random_json_file_from_dir loc
                        |> Image.image_from_json
                           


  let choose_image sources =
    let pair x = (x.use_frequency,x) in
    let source = let open Bg in
                     Random.wt_choose
                       (CCList.map pair (CCList.filter source_not_empty_p sources))
    in
    choose_image_from_source source
  
end
