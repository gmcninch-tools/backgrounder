
module History: sig
    
  type t = {image: Image.t;
            display_time: Core.Time_float.t}

  val history_encoder: t Decoders_ezjsonm.Encode.encoder
  val history_decoder: t Decoders_ezjsonm.Decode.decoder   
  val history_of_image: ?display_time:Core.Time_float.t -> Image.t -> t

  val history: unit -> t list Lwt.t
  val history_log: Image.t -> Image.t Lwt.t
  val write_history: t list -> unit Lwt.t
  val append_history: Image.t -> Image.t Lwt.t
end

module Potd: sig
  
  module Dj = Decoders_ezjsonm.Decode

  val all_potd_dates: Core.Date.t list
  
  val potd_query_d: (string list) Dj.decoder
  val potd_image_d: string -> Core.Date.t -> Image.t list Dj.decoder
  (* val potd_names: Core.Date.t -> string list Lwt.t *)
  val potd_image_list: Core.Date.t -> Image.t list Lwt.t

  exception Potd_no_image of Core.Date.t

  val potd_image: Core.Date.t -> Image.t Lwt.t
  val count_available: unit -> int
  val refill: int -> unit -> unit Lwt.t

end

module Chooser:sig

  val random_image_file_from_dirs: string list -> string
  val random_json_file_from_dir: string -> string
  val choose_image_from_source: Bg.source -> Image.t Lwt.t
  val choose_image: Bg.source list -> Image.t Lwt.t
  
end
