(* Time-stamp: <2023-03-08 Wed 22:16 EST - george@valhalla> *)

open Base
(* open Core_unix *)

type t = { image_source: source;
           image_name: string;
           image_dirname: string;}
and source = Local of string | Wmc_potd of potd
and potd = { date: Core.Date.t;
             url: string;
             desc_url: string;
           }

let source_encoder =
  let open Decoders_ezjsonm.Encode in
  fun source ->
  match source with
  | Local category -> obj [ "tag", string "local";
                            "value", value @@ string category
                        ]
  | Wmc_potd potd -> obj [ "tag", string "potd";
                           "value", obj [ ("date", string (Core.Date.to_string potd.date));
                                          ("url", string potd.url);
                                          ("desc_url", string potd.desc_url)
                                      ]
                       ]


(* let source_encoder = *)
(*   let open Decoders_ezjsonm.Encode in *)
(*   fun source -> *)
(*   match source with *)
(*   | Local category -> value @@ string category *)
(*   | Wmc_potd potd -> obj [ ("date", string (Core.Date.to_string potd.date)); *)
(*                            ("url", string potd.url); *)
(*                            ("desc_url", string potd.desc_url); *)
(*                        ] *)


let image_encoder =
  let open Decoders_ezjsonm.Encode in
  fun image ->
  obj [ ("source", source_encoder image.image_source);
        ("image_name", string image.image_name);
        ("image_dirname", string image.image_dirname);
    ]

let source_decoder  =
  let open Decoders_ezjsonm.Decode in
  let potd_decoder =
    let* sdate = field "date" string in
    let* url = field "url" string in
    let* desc_url = field "desc_url" string in
    let date = Core.Date.of_string sdate in
    succeed @@ {date; url; desc_url }
  in
  let* tag = field "tag" string in
  match tag with
  | "local" -> let* s = field "value" string in succeed @@ Local s
  | "potd" -> let* potd = field "value" potd_decoder in succeed @@ Wmc_potd potd
  | _ -> fail "expected local or potd image when parsing json"


(* let source_decoder  = *)
(*   let open Decoders_ezjsonm.Decode in *)
(*   let local_decoder = *)
(*     string >>= fun category -> *)
(*     succeed @@ Local category *)
(*   in *)
(*   let potd_decoder = *)
(*     field "date" string >>= fun sdate -> *)
(*     field "url" string >>= fun url -> *)
(*     field "desc_url" string >>= fun desc_url -> *)
(*     let date = Core.Date.of_string sdate in *)
(*     succeed @@ Wmc_potd {date; url; desc_url } *)
(*   in *)
(*   one_of *)
(*     [ ("local", local_decoder); *)
(*       ("potd", potd_decoder)] *)



let image_decoder =
  let open Decoders_ezjsonm.Decode in
  field "source" source_decoder >>= fun image_source ->
  field "image_name" string >>= fun image_name ->
  field "image_dirname" string >>= fun image_dirname ->
  succeed { image_source;
            image_name;
            image_dirname;
    }
            


let local_filename image =
  let open Core.Filename in
  concat image.image_dirname image.image_name

let local_json_filename image =
  let open Core.Filename in
  concat image.image_dirname (image.image_name ^ ".json")


let write_image_json image =
  let open Decoders_ezjsonm.Encode in
  let open Ezjsonm in
  let v = encode_value image_encoder image in
  let s = value_to_string ~minify:false v in
  let open Lwt_io in
  with_file
    ~mode:Output
    (local_json_filename image)
    (fun oc -> Lwt_io.write oc s)

let image_from_json fn =
  let open Decoders_ezjsonm.Decode in
  Lwt_io.with_file
    ~mode:Input
    fn
    (fun ic -> let%lwt s = Lwt_io.read ic in
               let v = decode_string image_decoder s in
               match v with
               | Error _ -> Lwt.fail @@ Failure "failed to parse image description"
               | Ok vv -> Lwt.return vv )
  


let display_image delay image =
  let prog = "feh" in
  let argv = [ prog; "--bg-max"; local_filename image ] in
  let pid = Core_unix.fork_exec ~prog ~argv () in
  ignore ( Core_unix.waitpid  pid : Core_unix.Exit_or_signal.t );
  let%lwt () = Lwt_unix.sleep delay in
  match image.image_source with
  | Local _ -> Lwt.return ()
  | Wmc_potd _ -> Lwt.return (
                      image |> local_filename |> Sys_unix.remove ;
                      image |> local_json_filename |> Sys_unix.remove ;
                                  )




(* let display_image delay image = *)
(*   let open Bg in *)
(*   let config = get_config () in *)
(*   let run ()  = *)
(*     match config.desktop_environment with *)
(*     | Mate -> Util.Mate_setter.set_bg_image @@ local_filename image *)
(*     | Cinnamon -> Util.Cinnamon_setter.set_bg_image @@ *)
(*                     String.concat ~sep:"" ["file:///";  (local_filename image)] *)
(*     | Gnome -> Util.Gnome_setter.set_bg_image @@ *)
(*                  String.concat ~sep:"" ["file:///";  (local_filename image)]               *)
(*     (\* | XFCE -> Util.Xfce_setter.set_bg_image @@ local_filename image *\) *)
(*     | Generic -> Util.Generic_setter.set_bg_image @@ local_filename image *)
(*   in *)
(*   run (); *)
(*   let%lwt () = Lwt_unix.sleep delay in *)
(*   match image.image_source with *)
(*   | Local _ -> Lwt.return () *)
(*   | Wmc_potd _ -> Lwt.return ( *)
(*                       image |> local_filename |> Sys_unix.remove ; *)
(*                       image |> local_json_filename |> Sys_unix.remove ; *)
(*                                   ) *)



