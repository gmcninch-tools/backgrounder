(* Time-stamp: <2021-08-29 Sun 15:02 EDT - george@valhalla> *)

type t = { image_source: source;
           image_name: string;
           image_dirname: string;}
and source = Local of string | Wmc_potd of potd
and potd = { date: Core.Date.t;
             url: string;
             desc_url: string;
           }


val image_encoder:t Decoders_ezjsonm.Encode.encoder
val image_decoder:t Decoders_ezjsonm.Decode.decoder
val local_filename:t -> string
val local_json_filename:t -> string
val write_image_json: t -> unit Lwt.t
val image_from_json: string -> t Lwt.t
val display_image: float -> t -> unit Lwt.t


