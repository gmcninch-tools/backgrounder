(* Time-stamp: <2023-03-08 Wed 20:10 EST - george@valhalla> *)

module Date_range = struct
  type t = Core.Date.t list
  let today () = Core.Date.today ~zone:(Lazy.force Timezone.local)
  let range date_1 date_2 =
    Core.Date.dates_between ~min:date_1 ~max:date_2
  let since date =
    range date (today ())
end

module type Desktop = sig
  val args: string -> string -> string list
  val prog: string
  val init_vals: unit -> (string*string) list
  val image_key: unit -> string
end

module type Setter = sig
  val set_option: string * string -> unit
  val set_bg_image: string -> unit
  val init_bg: (string*string) list ->  unit
end

module Make_setter(Desktop:Desktop):Setter = struct
  let set_option (key, value) =
    let argv = Desktop.args key value in 
    let pid  = Core_unix.fork_exec ~prog:Desktop.prog ~argv () in
    ignore ( Core_unix.waitpid  pid : Core_unix.Exit_or_signal.t )
  let init_bg args =
    Core.List.iter args ~f:(fun x-> set_option x)
  let set_bg_image image_file =
    set_option (Desktop.image_key (),image_file)
end

module Gnome = struct
  let args key value =
    [ "gsettings"; "set"; "org.gnome.desktop.background"; key; value ] 
  let prog         = "gsettings"
  let image_key () = "picture-uri"
  let init_vals () = [("picture-options","scaled")]
end

module Cinnamon = struct
  let args key value =
    [ "gsettings"; "set"; "org.cinnamon.desktop.background"; key; value ] 
  let prog         = "gsettings"
  let image_key () = "picture-uri"
  let init_vals () = [("picture-options","scaled")]
end

module Mate = struct
  let args key value =
    [ "gsettings"; "set"; "org.mate.background"; key; value ] 
  let prog         = "gsettings"
  let image_key () = "picture-filename"
  let init_vals () = [("picture-options","scaled")]
end

module Generic = struct
  let args _ _ = []
  let prog = "feh"
  let image_key () = "--bg-center"
  let init_vals () = []
end 

(* module XfceSupport = struct *)
(*   module Dj = Decoders_ezjsonm.Decode *)
  
(*   type spec = {image_key: string; *)
(*                init_vals: (string * string) list  *)
(*               } *)

(*   type t = (string * spec)  *)
  
(*   let kv_decoder = *)
(*     let open Dj in *)
(*     field "value" string >>= fun key -> *)
(*     field "key" string >>= fun value -> *)
(*     succeed ( key, value ) *)
  
(*   let spec_decoder = *)
(*     let open Dj in *)
(*     field "image_key" string >>= fun image_key -> *)
(*     field "init_vals" (list kv_decoder) >>= fun init_vals -> *)
(*     succeed { image_key; init_vals } *)

(*   let t_decoder = *)
(*     let open Dj in *)
(*     field "system" string >>= fun system -> *)
(*     field "spec" spec_decoder >>= fun spec -> *)
(*     succeed ( system, spec ) *)


(*   let get_config ?(filename="/home/george/assets/config/backgrounder/displays.json") () = *)
(*     let open Dj in *)
(*     let open Base.List.Assoc in     *)
(*     let host = Core_unix.gethostname () *)
(*     in *)
(*     let conf = decode_value (list t_decoder) @@ Ezjsonm.from_channel @@ Stdio.In_channel.create filename *)
(*     in *)
(*     match conf with *)
(*     | Ok r -> find_exn r ~equal:String.equal host *)
(*     | Error e -> pp_error Stdlib.Format.err_formatter e; *)
(*                  Stdlib.Format.(fprintf err_formatter "\nin file: %s\n" filename); *)
(*                  failwith "failed" *)

(* end *)

(* module Xfce = struct *)
(*   open XfceSupport  *)
      
(*   let args key value = *)
(*     [ "xfconf-query"; "-c"; "xfce4-desktop"; "-p"; key; "-s";  value ]  *)
(*   let prog      = "xfconf-query" *)

(*   let image_key () = *)
(*     (get_config ()).image_key *)

(*   let init_vals () = *)
(*     (get_config ()).init_vals *)
  
(*   (\* let image_key () = match Core.Unix.gethostname () with *)
(*    *   | "calliope" -> "/backdrop/screen0/monitoreDP-1/workspace0/last-image" *)
(*    *   | "cactus"   -> "/backdrop/screen0/monitorVGA-1/workspace0/last-image" *)
(*    *   | "valhalla" -> "/backdrop/screen0/monitorHDMI-0/workspace0/last-image" *)
(*    *   | _          -> "/backdrop/screen0/monitor0/workspace0/last-image" *)
(*    * let init_vals () = match Core.Unix.gethostname () with *)
(*    *   | "calliope" -> [("/backdrop/screen0/monitoreDP-1/workspace0/image-style","4")] *)
(*    *   | "cactus"   -> [("/backdrop/screen0/monitorVGA-1/workspace0/image-style","4")] *)
(*    *   | "valhalla" -> [("/backdrop/screen0/monitorHDMI-0/workspace0/image-style","4")] *)
(*    *   | _          -> [("/backdrop/screen0/monitor0/workspace0/image-style","4")] *\) *)


(* end   *)

module Gnome_setter    = Make_setter(Gnome)
module Cinnamon_setter = Make_setter(Cinnamon)
(* module Xfce_setter     = Make_setter(Xfce) *)
module Mate_setter     = Make_setter(Mate)
module Generic_setter  = Make_setter(Generic)

let image_suffixes  = ["jpe*g"; "gif"; "png"; "svg"]

let image_file_p filename =
  let rr = ".*(" ^ (Core.String.concat ~sep:"|" image_suffixes) ^ ")$"  
           |> Re.Pcre.re ~flags:[`CASELESS]
           |> Re.compile
  in
  match Core.Filename.split_extension filename with
  | _, Some x -> (not (Core.List.is_empty (Re.matches rr x)))
  | _,None -> false


let lsf d = 
  Core.List.map ~f:(Core.Filename.concat d) @@ Sys_unix.ls_dir d

let images_in_dir dirname =
  Core.List.filter ~f:image_file_p @@ lsf dirname

let json_file_p filename =
  Core.Filename.check_suffix filename "json"

let json_in_dir dirname =
  Core.List.filter ~f:json_file_p @@ lsf dirname


module Random = struct

  let transpose i j ll =
    let ith = CCList.nth_opt ll i in
    let jth = CCList.nth_opt ll j in
    let swappable = CCOption.(is_some ith && is_some jth) in
    let replace k _ =
      let kth = CCList.nth ll k in
      if k=i && swappable then  CCOption.value ~default:kth jth
      else if k=j && swappable then CCOption.value ~default:kth ith
      else kth
    in
    CCList.mapi replace ll

  let rec permute ll =
    let gen = CCRandom.int_range 0 @@ CCList.length ll in
    match transpose 0 (CCRandom.run gen) ll with
    | [] -> []
    | hd::tl -> hd::(permute tl)
  
  
  let random_sublist ll k =
    CCList.take k @@ permute ll


  let wt_choose flist =
    let rec rmatch fl r =
      match fl with
      | [] -> failwith "no match!"
      | [(_,y)] ->  y
      | (x,y)::tl -> if Core.Float.(r < x) then y else rmatch tl r
    in
    let r = CCRandom.(float 1. |> run)  in
    let tot =
      CCList.map (fun (x,_) -> x)  flist
      |> fun fl -> CCList.fold_left  (+.) 0. fl
    in
    let (_,ml) = CCList.fold_map 
                   (fun init (x,y) -> let inc = init +. (x /. tot) in
                                      (inc,(inc, y)))
                   0.
                   flist
    in
    rmatch ml r
end

