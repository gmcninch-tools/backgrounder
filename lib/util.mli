(* Time-stamp: <2023-03-08 Wed 20:07 EST - george@valhalla> *)

module Date_range: sig
  
  type t = Core.Date.t list
  val today : unit -> Core.Date.t
  val range : Core.Date.t -> Core.Date.t -> t
  val since : Core.Date.t -> t
  
end

module type Desktop = sig

  val args: string -> string -> string list
  val prog: string
  val init_vals: unit ->  (string*string) list
  val image_key: unit -> string

  
end

module type Setter = sig
  val set_option: string * string -> unit
  val set_bg_image: string -> unit
  val init_bg: (string*string) list ->  unit
end

module Make_setter(Desktop:Desktop):Setter

module Gnome:Desktop
module Cinnamon:Desktop
module Mate:Desktop
module Generic:Desktop

(* module XfceSupport:sig *)
(*   module Dj = Decoders_ezjsonm.Decode *)
  
(*   type spec = {image_key: string; *)
(*                init_vals: (string * string) list  *)
(*               } *)

(*   type t = (string * spec)  *)

(*   val kv_decoder: (string*string) Dj.decoder *)
(*   val spec_decoder: spec Dj.decoder  *)
(*   val t_decoder: t Dj.decoder *)
(*   val get_config: ?filename:string -> unit -> spec *)

(* end *)

(* module Xfce:Desktop *)

module Gnome_setter:Setter
(* module Xfce_setter:Setter *)
module Cinnamon_setter:Setter
module Mate_setter:Setter
module Generic_setter:Setter

val image_suffixes: string list
val image_file_p: string -> bool
val lsf:string -> string list
val images_in_dir: string -> string list
val json_file_p: string -> bool
val json_in_dir: string -> string list

module Random:sig
  val transpose: int -> int -> 'a list -> 'a list
  val permute: 'a list -> 'a list
  val random_sublist: 'a list -> int -> 'a list
  val wt_choose: (float * 'a) list -> 'a
end
