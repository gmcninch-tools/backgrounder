(* Time-stamp: <2021-04-09 Fri 13:35 EDT - george@valhalla> *)

open Core
open Base


include Util


module Wm_urls = struct
  let base_api_url = Uri.of_string "https://commons.wikimedia.org/w/api.php"

  let wm_images_query_url date =
    let date_s = Date.to_string date in
    Uri.add_query_params base_api_url [("action" , ["query"]);
                                       ("format" , ["json"]);
                                       ("prop"   , ["images"]);
                                       ("titles" , [String.concat ["Template:Potd/"; date_s; " (en)"]])]


  let wm_imageinfo_query_url filename =
    Uri.add_query_params base_api_url [("action" , ["query"]);
                                       ("format" , ["json"]);
                                       ("prop"   , ["imageinfo"]);
                                       ("iiprop" , ["url"]);
                                       ("titles" , [filename])]
end


module Web = struct

  let getbody url =
    let%lwt (_,body) = Cohttp_lwt_unix.Client.get url in
    Lwt.return body
  
  (* let getbody url =
   *   Cohttp_lwt_unix.Client.get url >>= fun ( _ , body) ->
   *   Cohttp_lwt.Body.to_string  body *)
  
  let download_url ~url ~fn =
    let%lwt b = getbody url in
    match b with
    | `Empty -> Lwt.return (Result.Error "no file downloaded")
    | _ -> Stdio.print_endline @@ "Downloaded " ^ fn;
           let%lwt () =
             Lwt_io.with_file 
               ~mode:Output fn
               (fun oc -> let%lwt s = Cohttp_lwt.Body.to_string b in
                          Lwt_io.write oc s )
           in
           Lwt.return (Result.Ok ())

  exception No_json of string
  
  let get_json url =
    let%lwt b = getbody url in
    match b with
    | `Empty -> No_json (Stdlib.Format.sprintf "No json in %s" (Uri.to_string url))
                |> Lwt.fail 
    | _ -> let%lwt s = Cohttp_lwt.Body.to_string b in
           Lwt.return (Ezjsonm.value_from_string s)
    
end



