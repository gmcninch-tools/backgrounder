(* Time-stamp: <2021-04-09 Fri 13:35 EDT - george@valhalla> *)

module Wm_urls:sig
  val base_api_url: Uri.t
  val wm_images_query_url: Core.Date.t -> Uri.t
  val wm_imageinfo_query_url: string -> Uri.t
end

module Web:sig
  val getbody: Uri.t -> Cohttp_lwt.Body.t Lwt.t
  val download_url: url:Uri.t -> fn:string -> ((unit,string) Result.t) Lwt.t
  val get_json: Uri.t -> Ezjsonm.value Lwt.t
end

